# MY AWESOME 3088 PIHAT PROJECT

Design and construction of a project using a microPIHAT for 3088 course

Our microhat design is designed to be a UPS module that is designed to be
attached to the PI zero and provide power in case of a power shortage. The
power to be provided by the microhat is expected to be enough to keep the PI
zero in activity even 2.5 hours after the event of a power shortage has
occurs. So, in this same sense the microhat will provide enough power for
the PI zero to keep functioning and evantually shut down in the conventional
way,after the power outrage has occur.

There are three possible user cases for this microhat design:

User role/Scenario1

In the event of a power outage, the Pi zero will shutdown in an unsafe manner and can damage the SD card module.
This is undesirable for the user and they would more than likely want to prevent 
this from occurring. Using or microHAT, the user can either power his/her board
through a power outage or safely shutdown their board hence preventing an unexpected 
immediate shutdown of the board. This in turn will prevent possible damage to 
the SD card module. Possible requirements for this board will include:
● R1.1: The microHAT must provide power to the correct PI level.
● R1.2: The microHAT must provide power long enough to shutdown the PI zero safely.
● R1.3: The microHAT must have a rechargeable power module for reuse-ability
● R1.4: The power supply must switch instantly from mains to back-up.
● R1.5: The microHAT must alert the PI that power has been lost or a command must be created,
 using software, to detect whether mains is available.
● R1.6: The microHAT must comply to the standard PiHAT board.
● R1.7: The microHAT should have a back-up ZVD circuit between 5V power pins
 and the 5V back-powering pins
● R1.8: Follows basic HAT GPIO requirements. 

User role/Scenario2

In some cases, user may want to take their Pi modules with them where mains is
not available for some period of time or an extended period of time. 
Or long term design can be used to power the PI zero for this duration of time
, or at least a sizeable part of the period, without mains connection. 
This would allow the user to make use of their PI wherever they go as long as 
they remembered to charge the energy module. (This design would make use of the 
long-term solution and hence will not make use of the short-term shutdown)
● R2.1: The microHAT should have an energy module that can last a few hours,
 much like a laptop battery.
● R2.2: The microHAT must provide power at the correct PI levels.
● R2.3: The microHAT must have a rechargeable power module for reuse-ability 
● R2.4: The microHAT should be able to measure battery level and inform user of 
 low battery.
● R2.5: Battery connectors for energy module should be able to handle the
 current that the PI will draw in worst case.
● R2.6: The microHAT must comply to the standard PiHAT board.
● R2.7: The microHAT should have a back-up ZVD circuit between 5V power pins
 and the 5V back-powering pins
● R2.8: Follows basic HAT GPIO requirements.

User role/Scenario3

Pi zeros can be  used as a personal server should the user desired to set them up as one for 
their use.It is non-ideal for a server to be shutdown because of a power outage.
It is therefore desirable to havea back-up energy module (UPS) to keep the
server alive, regardless of the presence of mains power. This module caters 
for this type of user with the long-term energy solution. The user can attach
the HAT to their PI module and can rest easy knowing that their server will not
unexpectedly shutdown due to a power outage.
● R3.1: The microHAT should have an energy module with a large capacity to last 
 a power outage.
● R3.2: The microHAT energy module should be extendable so that the user can add
 further capacity if power outage period lengths increase or are more frequent..
● R3.3: The microHAT should provide power at the correct PI levels
● R3.4: The microHAT must have a rechargeable power module for reuse-ability 
● R3.5: The microHAT should be able to measure battery level and inform user of
 low battery. Additionally the microHAT could also initiate an emergency shutdown
 procedure should the battery level fall to critical.
● R3.6: Battery connectors for energy module should be able to handle the current
 that the PI will draw in worst case.
● R3.7: The microHAT must comply to the standard PiHAT board.
● R3.8: The microHAT should have a back-up ZVD circuit between %V power pins and
 the 5V back-powering pins
● R3.9: Follows basic HAT GPIO requirements.